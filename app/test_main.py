from fastapi.testclient import TestClient

from .main import app

client = TestClient(app)


def test_read_main():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"docs": "/docs"}

def test_upload_csv_file():
    response = client.post(
        "/upload/",
        headers={"Content-Type": "multipart/form-data"},
        files={"file": "@titanic.csv;type=text/csv"}
    )
    assert response.status_code == 200