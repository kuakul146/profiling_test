from typing import Union

from fastapi import FastAPI , File, UploadFile
from fastapi.responses import FileResponse
import pandas as pd

from ydata_profiling import ProfileReport

import uuid

app = FastAPI()


@app.get("/")
def read_root():
    return {"docs": "/docs"}


@app.post("/upload")
def upload(file: UploadFile = File(...)):
    df = pd.read_csv(file.file)
    profile = ProfileReport(df, title="Profiling Report")
    filename = str(uuid.uuid4())
    file_path = "/tmp/"+filename+".html"
    profile.to_file(file_path)
    return FileResponse(path=file_path,filename=file_path,media_type='application/html')
